// bird feeder
// Dirk Weller
// CC BY SA 4.0 Int

// variables

rad=200;
wall=7;
hole1=120;
hole2=120;
bottomhole=5;
edge1=20;

// resolution settings

$fn=250;

// hole in bottom
// has to be in outer difference to make hole into bottom plate

difference () {

// bird feeder itself
    
union() {
 
// hanger thingy
    
difference()
    {
    translate([0,0,rad-5]) cylinder (h=25,r=15);
    translate([-15,0,rad+7]) rotate([0,90,0]) cylinder (h=40,r=bottomhole);
    }

// bird feeder hull and empty space 
    
difference() {
	color("grey")
    // hull 
    {
        sphere (r=rad);
     }
	// empty space inside
     sphere (r=rad-wall);
     
    // hole1 in bird feeder walls
    translate([0,rad,0]) rotate ([90,0,0]) cylinder (h = rad*3, r=hole1);
    // hole2 in bird feeder walls
    translate([-rad,0,0]) rotate ([0,90,0]) cylinder (h = rad*3, r=hole2);
    
 }
 
 // bottom plate
 color ("grey") translate([-rad,-rad,-rad]) cube([2*rad,2*rad,wall]);

}

// water exit hole in bottom
    translate([0,0,-rad*1.3]) rotate ([0,0,90]) cylinder (h = rad, r=bottomhole);

// holes in bottom plate

color ("red") translate([-rad+edge1,-rad+edge1,-rad-edge1+5]) cube([rad/5,rad*2-edge1*2,wall+edge1]);
color ("red") translate([rad-3*edge1,-rad+edge1,-rad-edge1+5]) cube([rad/5,rad*2-edge1*2,wall+edge1]);
}